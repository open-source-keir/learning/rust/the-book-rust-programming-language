// Bring io library into scope
use std::io;
use rand::Rng;
use std::cmp::Ordering; // Ordering is an enum (Less, Greater, Equal)

fn main() {
    // Ask for user input
    println!("Guess the number!");
    println!("Please input your guess: ");

    // Generate a number using the particular rand::thread_rng generator
    // - Local to current execution thread
    // - Seeded by the OS
    let secret_number = rand::thread_rng()
        .gen_range(1, 101); // Call gen_range() method to generate a random number

    // Create mutable String (UTF-8 encoded bits) to hold user input text
    // let -> used to create a variable
    // mut -> indicates variable is mutable (default is immutable)
    // :: -> indicates that new is an associated function of the type String
    let mut input_guess = String::new();

    // & -> indicates argument is a reference
    // &mut -> makes the reference data mutable (alternative would be &input_guess
    io::stdin()
        // Returns a Result enum (Ok or Err) that encodes error handling information
        // Ok -> operation successful, contains generated value
        // Err -> operation failed, contains info about why
        .read_line(&mut input_guess) // Returns a Result enum (Ok or Err)
        .expect("Failed to read line!"); // Returns this if Err, else returns Ok generated value

    // {} -> value placeholder
    print!("You guessed: {}", input_guess);

    // cmp method compares two values and can be called on anything that can be compared
    // cmp takes a reference to whatever is being compared with the value (value.cmp(reference))
    match secret_number.cmp(&secret_number) {
        Ordering::Less => println!("Too small!"),
        Ordering::Greater => println!("Too big!"),
        Ordering::Equal => println!("Winner winner chicken dinner!"),
    }

}
